<?php 
require __DIR__ . "/vendor/autoload.php";

use Goutte\Client;

function getArtistPicture($artist) 
{
	$url = 'https://www.last.fm/music/' . $artist; 

	$client = new Client();

	$crawler = $client->request('GET', $url);

	$output = $crawler->filter('header > div')->extract('style');
	// $regex = '/\["background-image: url\((.*)\)/';
	$regex = '/url\((.*)\)/';
		
	preg_match($regex, $output[0], $match);
    
  	$pictureName =  $artist . '_Profile.jpeg';

	copy($match[1],'../snare/static/lastfm/'. $pictureName);
		//copy($match[1], $path);
}


$artistList = [
	'Radiohead',
	'Deafheaven',
	'Elliott Smith',
	'Danny Brown',
	'Burial',
	'american football',
	'big L',
	'black Flag',
	'Car Seat Headrest',
	'Cibo Matto',
	'Megadeth',
	'La Dispute',
	'Kyuss',
	'Queens of the Stone Age',
	'Minor Threat',
	'Peeping Tom',
	'The Velvet Underground',
	'Talking Heads',
	'Raekwon',
	'Quasimoto',
	'Neurosis',
	'Gorillaz',
	'Slowdive',
	'Shlohmo',
	'Metallica'
];

$seedPictures = array_map("getArtistPicture", $artistList);


?>
